/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReadAndWriteIO;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


/**
 *
 * @author Joni Kettunen op.num.0439302
 * Zip tiedostokäsittely lähde: http://www.dreamincode.net/forums/topic/244086-write-and-read-zip-file-from-java/
 */
public class ReadAndWriteIO {


    private String inputFileName = "turhuus";
    private String outputFilename = "turhuus";
    private String zipInput = "turhuus";
    
    public ReadAndWriteIO(String i,String o,String z){
        inputFileName = i;
        outputFilename = o;
        zipInput = z;
    }
    public void readUsingZipFile() throws IOException {
        ZipInputStream in = new ZipInputStream(new FileInputStream(zipInput));
        
        ZipEntry entry = in.getNextEntry();
        
        while(entry != null){
            byte[] buf = new byte[1024];
            int len = in.read(buf);
            String theFile = new String(buf, 0, len);
            System.out.println(theFile);
            entry = in.getNextEntry();
            }
        in.close();
        
        }

    
    
    public String readFile() throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(inputFileName));
        String s = in.readLine();
        in.close();
        return s;
    }
    
    public void writeText(String text) throws FileNotFoundException, IOException{
        BufferedWriter out = new BufferedWriter(new FileWriter(outputFilename));
        out.write(text);
        out.close(); 
    }
    
    public void readAndWrite() throws FileNotFoundException, IOException{
        BufferedReader in = new BufferedReader(new FileReader(inputFileName));
        BufferedWriter out = new BufferedWriter(new FileWriter(outputFilename));
        String s = in.readLine();
        String a = s;
        while(s != null){
            a = a.replaceAll("\\s","");
            if(s.length() < 30 && a.isEmpty() == false && s.contains("v") == true){
                out.write(s + "\n");
            }
            s = in.readLine();
        a = s;           
        }
        out.close();
        in.close();
    }
}