/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReadAndWriteIO;

import java.io.IOException;

/**
 *
 * @author Joni Kettunen op.num. 0439302
 */
public class Mainclass {
    
    public static void main(String[] args) throws IOException, ClassNotFoundException {
    
        String fileName1 = "input.txt";
        String fileName2 = "output.txt";  
        String fileName3 = "zipinput.zip";

    
        ReadAndWriteIO IO1 = new ReadAndWriteIO(fileName1,fileName2,fileName3);

    IO1.readUsingZipFile();
}
}
